create table tblpengarang
create table tblpengarang 
(
ID int primary key not null,
kd_pengarang varchar(7) not null,
nama varchar(30) not null,
alamat varchar(80) not null,
kota varchar(15) not null,
kelamin varchar(1) not null
)


create table tblgaji
(
	ID int primary key not null,
	kd_pengarang varchar(7) not null,
	nama varchar(30) not null,
	gaji Decimal(18,4) not null
)


    insert into tblpengarang
    (
        kd_pengarang,
        nama,
        alamat,
        kota,
        kelamin
    ) values
        ('P0001','Ashadi','Jl. Beo 25','Yogya','P'),
        ('P0002','Rian','Jl. Solo 123','Yogya','P'),
        ('P0003','Suwadi','Jl. Semangka 13','Bandung','P'),
        ('P0004','Siti','Jl. Durian 15','Solo','W'),
        ('P0005','Amir','Jl. Gajah 33','Kudus','P'),
        ('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
        ('P0007','Jaja','Jl. Singa 7','Bandung','P'),
        ('P0008','Saman','Jl. Naga 12','Yogya','P'),
        ('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
        ('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')

    insert into tblgaji
    (
        kd_Pengarang,
        nama,
        gaji
    ) values
        ('P0002','Rian','600000'),
        ('P0005','Amir','700000'),
        ('P0004','Siti','500000'),
        ('P0003','Suwadi','1000000'),
        ('P00010','Fatmawati','600000'),
        ('P0008','Saman','750000')

1. 	select count(nama) as Jumlah_Pengarang 
	from tblpengarang

2.  select count(a."kelamin") as jumlah, a."kelamin"
	from tblpengarang as a
	group by a."kelamin"

3.  select count(kota) as Jumlah_Kota, kota 
	from tblpengarang
	group by kota

4.  select count(kota) as Jumlah_Kota, kota 
	from tblpengarang
	group by kota
	having count(kota) > 1

5. 	select max(kd_pengarang) as terbesar, 
	min(kd_pengarang) as terkecil 
	from tblpengarang

6.  select max(gaji) as terbesar, 
	min(gaji) as terkecil 
	from tblgaji

7. select nama, gaji from tblgaji
   where gaji > 600000

8. select sum(gaji) as Jumlah_Gaji 
	from tblgaji

9. 	select CASE WHEN sum(b."gaji") is null 
	THEN '0' 
	ELSE sum(b."gaji")
	END AS total_gaji, a.kota from tblpengarang as a
	left join tblgaji as b
	on a."kd_pengarang" = b."kd_pengarang"
	group by a."kota"

10. select a."kd_pengarang", a."nama", a."alamat", a."kota", a."kelamin", b."gaji" 
	from tblpengarang as a
	right join tblgaji as b
	on a."kd_pengarang" = b."kd_pengarang"
	where a."kd_pengarang" between 'P0001' AND 'P0006'
	order by b."kd_pengarang"

11. select * from tblpengarang 
	where kota ='Yogya' OR kota ='Solo' OR kota ='Magelang'

12. select * from tblpengarang 
	where kota !='Yogya'

13. a. 	select * from tblpengarang 
		where nama like 'S%'
	b. 	select * from tblpengarang 
		where nama like '%i'
	c. 	select * from tblpengarang 
		where nama like '__a%'
	d. 	select * from tblpengarang 
		where nama not like '%!n'

14. select * from tblpengarang as a
	join tblgaji as b
	on a."kd_pengarang" = b."kd_pengarang"

15.	select a."kota", b."gaji" 
	from tblpengarang as a
	join tblgaji as b
	on a."kd_pengarang" = b."kd_pengarang"
	where b."gaji" < 1000000

16. alter table tblpengarang
    alter column kelamin TYPE varchar(10)

17. alter table tblpengarang
    add column gelar varchar(12)

18. UPDATE tblpengarang 
	SET alamat ='Jl. Cendrawasih 65', kota='Pekanbaru' 
	WHERE nama='Rian';

19. create View vwPengarang as select kd_pengarang, nama, kota 
	from tblpengarang
	
select * from tblpengarang
select * from tblgaji