create table JurusanKuliah 
		(	
		id int primary key not null,
		kodejurusan varchar(12) not null,
		deskripsi varchar(20) not null
		)
select * from JurusanKuliah

insert into JurusanKuliah 
		(kodejurusan,
		 deskripsi
		)values
		('KA001','Teknik Informatika'),
		('KA002','Management Bisnis'),
		('KA003','Ilmu Komunikasi'),
		('KA004','Sastra Inggris'),
		('KA005','Ilmu Pengetahuan Alam Dan Matematika'),
		('KA006','Kedokteran'
		)
		
		
		
create table Dosen 
		(	
		id int primary key not null,
		kodedosen varchar(14) not null,
		namadosen varchar(15) not null,
		kodematakuliah varchar(12) not null,
		kodefakultas varchar(13) not null
		)
		
select * from Dosen

insert into Dosen 
		(kodedosen,
		 namadosen,
		 kodematakuliah,
		 kodefakultas
		)values
		('GK001','Ahmad Prasetyo','KPK001','TK002'),
		('GK002','Hadi Fuladi','KPK002','TK001'),
		('GK003','Johan Goerge','KPK003','TK002'),
		('GK004','Bima Darmawan','KPK004','TK002'),
		('GK005','Gatot Wahyudi','KPK005','TK001'
		)
		
create table matakuliah (
id int primary key not null,
kodematakuliah varchar(12) not null,
namamatakuliah varchar(50) not null,
keaktifanstatus varchar(15) not null
)

select * from matakuliah


insert into matakuliah
(kodematakuliah, namamatakuliah, keaktifanstatus)
values
('KPK001','Algoritma Dasar','Aktif'),
('KPK002','Basis Data','Aktif'),
('KPK003','Kalkulus','Aktif'),
('KPK004','Pengantar Bisnis','Aktif'),
('KPK005','Matematika Ekonomi & Bisnis','Non Aktif')

create table mahasiswa (
id int primary key not null,
kodemahasiswa varchar(12) not null,
namamahasiswa varchar(20) not null,
alamatmahasiswa varchar(30) not null,
kodejurusan varchar(12) not null,
kodematakuliah varchar(12) not null
)

select * from mahasiswa

insert into mahasiswa
(kodemahasiswa, namamahasiswa, alamatmahasiswa, kodejurusan, kodematakuliah)
values
('MK001','Fullan','Jl. Hati besar no 63 RT.13','KA001','KPK001'),
('MK002','Fullana Binharjo','Jl. Biak kebagusan No. 34','KA002','KPK002'),
('MK003','Sardine Himura','Jl. Kebajian no 84','KA001','KPK003'),
('MK004','Isani Isabul','Jl. Merak merpati No.78','KA001','KPK001'),
('MK005','Charlie birawa','Jl. Air terjun semidi No.56','KA003','KPK002')



create table nilaimahasiswa (
id int primary key not null,
kodenilai varchar(12) not null,
kodemahasiswa varchar(12) not null,
kodeorganisasi varchar(9) not null,
nilai int not null
)

insert into nilaimahasiswa
(kodenilai, kodemahasiswa, kodeorganisasi, nilai)
values
('SK001','MK004','KK001',90),
('SK002','MK001','KK001',80),
('SK003','MK002','KK003',85),
('SK004','MK004','KK002',95),
('SK005','MK005','KK005',70)

select * from nilaimahasiswa

create table fakultas (
id int primary key not null,
kodefakultas varchar(13) not null,
penjelasan varchar(30) not null
)

insert into fakultas
(kodefakultas, penjelasan)
values
('TK001','Teknik Informatika'),
('TK002','Matematika'),
('TK003','Sistem Informatika')

select * from fakultas

create table organisasi (
id int primary key not null,
kodeorganisasi varchar(12) not null,
namaorganisasi varchar(50) not null,
statusorganisasi varchar(13) not null
)

insert into organisasi
(kodeorganisasi, namaorganisasi, statusorganisasi)
values
('KK001','Unit Kegiatan Mahasiswa (UKM)','Aktif'),
('KK002','Badan Eksekutif Mahasiswa Fakultas (BEMF)','Aktif'),
('KK003','Dewan Perwakilan Mahasiswa Universitas (DPMU)','Aktif'),
('KK004','Badan Eksekutif Mahasiswa Universitas (BEMU)','Non Aktif'),
('KK005','Himpunan Mahasiswa Jurusan','Non Aktif'),
('KK006','Himpunan Kompetisi Jurusan','Aktif'
)

select * from dosen
select * from mahasiswa
select * from jurusankuliah
select * from matakuliah


2.	alter table dosen
	alter column namadosen TYPE varchar(200)

3.	select a.kodemahasiswa, a.namamahasiswa, b.deskripsi
	from mahasiswa as a
	join jurusankuliah as b
	on a.kodejurusan = b.kodejurusan
	where kodemahasiswa = 'MK001'

4.	select a.namamatakuliah, a.keaktifanstatus, b.kodemahasiswa, 
	b.namamahasiswa, b.alamatmahasiswa, c.kodejurusan, a.kodematakuliah
	from matakuliah as a
	left join mahasiswa as b
	on a.kodematakuliah = b.kodematakuliah
	left join jurusankuliah as c
	on b.kodejurusan = c.kodejurusan
	where a.keaktifanstatus = 'Non Aktif'
	
	select * from organisasi
	select * from mahasiswa
	select * from nilaimahasiswa
	select * from matakuliah
	
5. 	select a.kodemahasiswa, a.namamahasiswa, a.alamatmahasiswa, c.kodeorganisasi, c.statusorganisasi
	from mahasiswa as a
	join nilaimahasiswa as b
	on a.kodemahasiswa = b.kodemahasiswa
	join organisasi as c
	on b.kodeorganisasi = c.kodeorganisasi
	where statusorganisasi = 'Aktif' AND nilai > 79

6.	select kodematakuliah, namamatakuliah, 
	keaktifanstatus
	from matakuliah
	where namamatakuliah like '%n%'

7.	select a.namamahasiswa, c.namaorganisasi
	from mahasiswa as a
	join nilaimahasiswa as b
	on a.kodemahasiswa = b.kodemahasiswa
	join organisasi as c
	on b.kodeorganisasi = c.kodeorganisasi
	order by c.namaorganisasi desc
	limit 2
	
	
	select * from organisasi
	select * from mahasiswa
	select * from nilaimahasiswa
	select * from matakuliah
	select * from fakultas
	select * from dosen
	select * from jurusankuliah
	
8.	select a.kodemahasiswa, a.namamahasiswa, b.namamatakuliah,
	d.deskripsi as jurusan, c.namadosen, b.keaktifanstatus as status_mata_kuliah, 
	penjelasan as deskripsi
	from mahasiswa as a
	join matakuliah as b
	on a.kodematakuliah = b.kodematakuliah
	join dosen as c
	on a.kodematakuliah = c.kodematakuliah
	join jurusankuliah as d
	on a.kodejurusan = d.kodejurusan
	join fakultas as e
	on c.kodefakultas = e.kodefakultas
	where a.kodemahasiswa = 'MK001'
	
9.	create View universitas as select a.kodemahasiswa, 
	a.namamahasiswa, b.namamatakuliah, 
	c.namadosen, b.keaktifanstatus, d.penjelasan
	from mahasiswa as a
	join matakuliah as b
	on a.kodematakuliah = b.kodematakuliah
	join dosen as c
	on b.kodematakuliah = c.kodematakuliah
	join fakultas as d
	on c.kodefakultas = d.kodefakultas
	
10.	select * from universitas
	where kodemahasiswa = 'MK001'


