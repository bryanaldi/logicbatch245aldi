		create table artis 
		(	
		kd_artis varchar(100) primary key not null,
		nama_artis varchar(100) not null,
		jk varchar(100) not null,
		bayaran integer not null,
		award integer not null,
		negara varchar(100) not null
		)

		select * from artis

		insert into artis 
		(kd_artis, 
		 nama_artis, 
		 jk, 
		 bayaran, 
		 award, 
		 negara
		)values
		('A001','ROBERT DOWNEY JR','PRIA',500000000,2,'AS'),
		('A002','ANGELINA JOLIE','PRIA',700000000,1,'AS'),
		('A003','JACKIE CHAN','PRIA',200000000,7,'HK'),
		('A004','JOE TASLIM','PRIA',350000000,1,'ID'),
		('A005','CHELSEA ISLAN','PRIA',300000000,0,'ID')

		create table negara (
		kd_negara varchar(100) primary key not null,
		nm_negara varchar(100) not null
		)

		select * from  negara

		insert into negara 
		(kd_negara,
		 nm_negara
		) values
		('AS','AMERIKA SERIKAT'),
		('HK','HONGKONG'),
		('ID','INDONESIA'),
		('IN','INDIA')

		create table genre (
		kd_genre varchar(50) primary key not null,
		nm_genre varchar(50) not null
		)

		insert into genre 
		(kd_genre, 
		 nm_genre
		) values
		('G001','ACTION'),
		('G002','HORROR'),
		('G003','COMEDY'),
		('G004','DRAMA'),
		('G005','THRILER'),
		('G006','FICTION')

		select * from genre


		create table produser (
		kd_produser varchar(50) primary key not null,
		nm_produser varchar(50) not null,
		international varchar(50)
		)

		insert into produser 
		(kd_produser, 
		 nm_produser, 
		 international
		) values
		('PD01','MARVEL','YA'),
		('PD02','HONGKONG CINEMA','YA'),
		('PD03','RAPI FILM','TIDAK'),
		('PD04','PARKIT','TIDAK'),
		('PD05','PARAMOUNT PICTURE','YA')

		select * from produser

		create table film (
		kd_film varchar(10) primary key not null,
		nm_film varchar(55) not null,
		genre varchar(55) not null,
		artis varchar(55) not null,
		produser varchar(55) not null,
		pendapatan integer not null,
		nominasi integer not null
		)

		insert into film 
		(kd_film, 
		 nm_film, 
		 genre, 
		 artis, 
		 produser, 
		 pendapatan, 
		 nominasi
		) values
		('F001','IRON MAN','G001','A001','PD01',2000000000,3),
		('F002','IRON MAN 2','G001','A001','PD01',1800000000,2),
		('F003','IRON MAN 3','G001','A001','PD01',1200000000,0),
		('F004','AVENGER : CIVIL WAR','G001','A001','PD01',2000000000,1),
		('F005','SPIDERMAN HOME COMING','G001','A001','PD01',1300000000,0),
		('F006','THE RAID','G001','A004','PD03',800000000,5),
		('F007','FAST & FURIOUS','G001','A004','PD05',830000000,2),
		('F008','HABIBIE DAN AINUN','G004','A005','PD03',670000000,4),
		('F009','POLICE STORY','G001','A003','PD02',700000000,3),
		('F010','POLICE STORY 2','G001','A003','PD02',710000000,1),
		('F011','POLICE STORY 3','G001','A003','PD02',615000000,0),
		('F012','RUSH HOUR','G003','A003','PD05',695000000,2),
		('F013','KUNGFU PANDA','G003','A003','PD05',923000000,5
		)

select * from film



1.	select nm_film as nama_film, nominasi as nom_terbesar from film
	where nominasi = (
   	select max (nominasi)
   	FROM film
	)
	
2.	select nm_film as nama_film, nominasi as nom_terbanyak from film
	where nominasi = (
   	select max (nominasi)
   	FROM film
	)

3.	select nm_film, nominasi
	from film
	where nominasi = 0;
	
4.	select nm_film as nama_film, pendapatan 
	from film
	where pendapatan = (
   	select min (pendapatan)
   	FROM film
	)

5.	select nm_film as nama_film, pendapatan 
	from film
	where pendapatan = (
   	select max (pendapatan)
   	FROM film
	)

6.	select * from film
	where nm_film like 'P%'
	
7.	select * from film
	where nm_film like '%H'
	
8. 	select * from film
	where nm_film like '%D%'

9.	select nm_film as nama_film, pendapatan as pendapatan_terbesar from film
	where pendapatan = (
   	select max (pendapatan)
   	FROM film
	) AND nm_film like '%O%'
	
	
	
10.	select nm_film as nama_film, pendapatan as pendapatan_terbesar from film
	where pendapatan = (
   	select min (pendapatan)
   	FROM film
	) AND nm_film like '%O%'


11.	select a."nama_artis", b."nm_film", b."artis" 
	from artis as a
	left join film as b
	on a."kd_artis" = b."artis"
	order by a."kd_artis"
	
12.	select a."nama_artis", a."negara"
	from artis as a
	join negara as b
	on a."negara" = b."kd_negara"
	where b."kd_negara" = 'HK'

13.	select a."nama_artis", b."artis", b."nm_film", c."nm_negara"
	from artis as a
	left join film as b
	on a."kd_artis" = b."artis"
	right join negara as c
	on a."negara" = c."kd_negara"
	where c."nm_negara" not like '%O%'
	
14.	select a."nama_artis", b."artis", b."nm_film"
	from artis as a
	left join film as b
	on a."kd_artis" = b."artis"
	where b."artis" IS NULL
	
15.	select a."artis", b."nm_genre", c."nama_artis"
	from film as a
	join genre as b
	on a."genre" = b."kd_genre"
	join artis as c
	on a."artis" = c."kd_artis"
	where b."nm_genre" = 'DRAMA'
	
	
16.	select a."artis", b."nm_genre", c."nama_artis"
	from film as a
	right join genre as b
	on a."genre" = b."kd_genre"
	right join artis as c
	on a."artis" = c."kd_artis"
	where b."nm_genre" = 'HOROR'

17.	select  count(c."nm_film") as Jumlah_film, a."negara"
	from artis as a
	right join negara as b
	on a."negara" = b."kd_negara"
	left join film as c
	on a."kd_artis" = c."artis"
	group by a."negara"

18. select  a."kd_produser", a."nm_produser", a."international", sum(b."pendapatan") as Total_Pendapatan
	from produser as a
	join film as b
	on a."kd_produser" = b."produser"
	where a."international" = 'YA'
	group by a."kd_produser"

19. select  a."kd_produser", a."nm_produser", count(b."nm_film") as Total_Pendapatan
	from produser as a
	join film as b
	on a."kd_produser" = b."produser"
	group by a."kd_produser" 


20. select   a."nm_produser", sum(b."pendapatan") as Total_Pendapatan
	from produser as a
	join film as b
	on a."kd_produser" = b."produser"
	where a."nm_produser" = 'MARVEL'
	group by a."nm_produser"
	
	
	


