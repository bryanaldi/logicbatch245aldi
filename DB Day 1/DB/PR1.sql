create table tblkaryawan 
(
ID int primary key not null,
nomor_induk varchar(7) not null,
nama varchar(30) not null,
alamat text not null,
tanggal_lahir date(15) not null,
tanggal_masuk date(1) not null
)

 insert into tblpengarang
    (
        kd_pengarang,
        nama,
        alamat,
        kota,
        kelamin
    ) values
        ('IP06001','Agus','Jln. Gajah Mada 115A, Jakarta Pusat','1/8/70','7/7/06'),
        ('IP06002','Amin','Jln. Bungur sari v No, 178, bandung','3/5/77','6/7/06'),
        ('IP06003','Yusuf','Jln. Yosodpuro 15, surabaya','9/8/73','8/7/06'),
        ('IP07004','Alyssa','Jln. Cendana No. 6 Bandung','14/2/83','5/1/07'),
        ('IP07005','Maulana','Jln. Ampera Raya No 1','10/10/85','2/5/07'),
        ('IP07006','Afika','Jln. Pejaten Barat No 6A','9/3/87','9/6/07'),
        ('IP07007','James','Jln. Padjadjaran No. 111, bandung','19/5/88','9/6/06'),
        ('IP09008','Octavanus','Jln. Gajah Mada 101. Semarang','7/10/88','8/8/08'),
        ('IP09009','Nugroho','Jln. Duren Tiga 196, Jakarta selatan','20/1/88','11/11/08'),
        ('IP09010','Raisa','Jln. Nangka Jakarta selatan','29/12/89','9/2/09')


create table cuti_karyawan
(
	ID int primary key not null,
	nomor_induk varchar(7) not null,
	tanggal_mulai date not null,
	lama_cuti int not null,
	keterangan text not null
)

    insert into cuti_karyawan
    (
        nomor_induk,
        tanggal_mulai,
        lama_cuti,
		keterangan
    ) values
        ('IP06001','1/2/12','3','Acara keluar'),
        ('IP06001','13/2/12','4','Anak sakit'),
        ('IP07007','15/2/12','2','Nenek sakit'),
        ('IP06003','17/2/12','1','Mendaftar sekolah anak'),
        ('IP07006','20/2/12','5','Menikah'),
        ('IP07004','27/2/12','1','Imunisasi anak')



select * from tblkaryawan
order by tanggal_masuk ASC

1.	select a."nama", a."tanggal_masuk"
	from tblkaryawan as a
	left join cutikaryawan as b
	ON a."nomor_induk" = b."nomor_induk"
	ORDER BY a."tanggal_masuk" ASC
	LIMIT 3
	
	SELECT nama, min(tanggal_masuk) AS min_date
	from tblkaryawan
	GROUP BY nama
	ORDER BY min_date
	LIMIT 3;
	

2.	select b."nomor_induk", a."nama", b."tanggal_mulai", b."keterangan" 
	from tblkaryawan as a
	join cutikaryawan as b
	ON a."nomor_induk" = b."nomor_induk"
	where b."tanggal_mulai" < to_date('2/16/12', 'MM/DD/YY')
	and b."tanggal_mulai" + b."lama_cuti" > to_date ('2/16/12', 'MM/DD/YY')
	
3.	select sum(b."lama_cuti"), b."nomor_induk", a."nama"
	from tblkaryawan as a
	join cutikaryawan as b
	ON a."nomor_induk" = b."nomor_induk"
	GROUP BY b."nomor_induk", a."nama"
	HAVING sum(b."lama_cuti") > 1


4.	select case when 12 - sum(lama_cuti) is null 
	then 12	else 12 - sum (lama_cuti)
	END AS sisa_cuti,
	a."nomor_induk", a."nama"
	from tblkaryawan as a
	left join cutikaryawan as b
	on a."nomor_induk" = b."nomor_induk"
	group by a."nomor_induk", a."nama"